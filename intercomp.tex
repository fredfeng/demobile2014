\subsection{Inter-component Control-flow Analysis}\label{sec:iccg}

We now describe the construction of the 
inter-component call graph (ICCG), which is used for
deciding ICC queries. 

\begin{definition} 
\vspace{-0.05in}
An ICCG for a program $P$ is a graph $(N, E)$ such that nodes $N$ are the set of components in $P$, and edges $E$ define a relation $E \subseteq  (N \times A  \times D  \times N)$  where $A$ and $D$ are the
domain of all actions and data types defined by the Android system augmented with the element $\bot$.
\vspace{-0.05in}
\end{definition}

In other words, ICCG is a directed graph where nodes are components in an application, and edges are labeled with actions and data types. 
%Intuitively, if the predicate \verb+icc(p, q, a, d)+
%from Section~\ref{sec:preds} is true, this means that $p, q \in N$ and $(p, q, a, d) \in E$. 
%We allow edge labels to be $\top$ because the actions or data types associated with intents may not be statically known (for instance, due to some complicated string manipulation). 
The special element $\bot$ indicates that the intent defining the ICC does not have action or data type information associated with it.
Given a callgraph and the results of the pointer analysis, Apposcopy constructs the ICCG by performing two key steps that we explain next.

\begin{table}
\vspace{-0.15in}
  \centering
  \caption{API for setting Intent attributes}
  \begin{tabular}{|c||l|}
      \hline
    Target  &
      {\small \begin{tabular}{l}
                   \texttt{ setComponent(ComponentName)}, \\
                   \texttt{ setClassName(Context, String)}, \\
                   \texttt{ setClassName(String, String)},\\
                   \texttt{ setClass(Context, Class)}\\ 
        \end{tabular} } \\
    \hline
    Action &
		  {\small \begin{tabular}{l}
                   \texttt{ setAction(String)}
                  \end{tabular} } \\
    \hline
    Data type &
		{\small \begin{tabular}{l}
                   \texttt{ setType(String)},
                   \texttt{ setData(URI)},\\
                   \texttt{ setDataAndType(URI,String)}\\
                   \end{tabular} } \\
    \hline
  \end{tabular}
\label{table:intentapis}
% % \vspace{-0.2in}
\vspace{-0.1in}
\end{table}

\subsubsection{Data Flow Analysis for Intents}
The first step of ICCG construction is a data flow analysis to track the information stored in Intent objects. Specifically, we track three kinds of information about intents:
\begin{itemize}
\item {\bf Target:} In the Android framework, a component can  specify the target of an Intent (and, hence, the ICC) by calling the methods shown in the first row of Table~\ref{table:intentapis}. Such intents whose targets have been explicitly specified are called \emph{explicit intents}.
\item {\bf Action:} A component can specify the action that the ICC target needs to perform by calling the methods shown in the second row of Table~\ref{table:intentapis}. 
\item {\bf Data type:} An application can specify the data type that the recipient component needs to operate on by calling the methods shown in the last row of Table~\ref{table:intentapis}. 
\end{itemize}

When a component does not specify the target of an intent explicitly, the Android system resolves the recipient components of the ICC based on  \emph{intent filters}   declared in the manifest file. An intent filter  specifies the kinds of actions that a component will respond to. For instance, consider the \verb+AndroidManifest.xml+ file from Figure~\ref{fig:icc-ex-manifest}. Here, 
%in lines 29-32, 
\verb+MsgAct+  declares that it responds to intents whose corresponding action and data type are \verb+SEND+ and \verb+text/plain+ respectively.


In order to build the ICCG, Apposcopy first performs a forward interprocedural dataflow analysis, called the \emph{intent analysis}, which overapproximates the target, action, and data type associated with each intent object. Specifically, for each variable \verb+i+ of type \verb+Intent+, our analysis tracks three variables $i_t, i_a$, and $i_d$ whose domains are sets of 
components, actions, and data types respectively.
We initialize the dataflow value for each variable to be $\{ \bot \}$ and define the join operator as set union. 


Table~\ref{table:intentapis} shows the set of  Android API methods for setting attributes of Intent objects. Since other methods do not  change attributes of intents, the transfer function for any statement not included in Table~\ref{table:intentapis} is the identity function.

Figure~\ref{fig:transfer-func} shows the transfer function for \verb+setComponent+ in the form of inference rules. Since transformers for the other statements from Table~\ref{table:intentapis} are very similar, we only focus on \verb+setComponent+ as a representative. In Figure~\ref{fig:transfer-func},  environment $\Gamma$ denotes data flow facts for targets of intent variables as a mapping from each variable to its corresponding dataflow value. Now, consider the statement
{\tt 
x.setComponent(s)
}
where \verb+x+ is a variable of type Intent and \verb+s+ is a string specifying the target component. If another variable \verb+y+ is an alias of \verb+x+, then the target  of \verb+y+ will also be affected by this statement. Hence, our transfer function must update the data flow values of all variables that are aliases of \verb+x+. Now, there are three cases to consider. If \verb+x+ and \verb+y+ are guaranteed to be aliases, as in the first rule of Figure~\ref{fig:transfer-func},  then we can kill its old value and update its new value to $\{ s\}$ (i.e., a strong update). In the second rule, if \verb+y+ \emph{may} alias \verb+x+, then \verb+y+'s target could either remain unchanged or become \verb+s+; hence, we only perform a weak update. Finally, if \verb+y+ and \verb+x+ do not alias, then \verb+y+'s target definitely remains unchanged and its existing dataflow values are preserved. The \emph{may\_alias} and \emph{must\_alias} relations used in Figure~\ref{fig:transfer-func} are defined according to Figure~\ref{fig:alias}.
 
 \begin{figure}
 \end{figure}
 

 
  Based on the results of this data flow analysis, we can now determine whether an intent is explicit or implicit. Specifically, if $\Gamma(x_t)$ does not contain $\bot$,  the target of the intent must have been explicitly specified. Hence, we write $\emph{explicit}(x)$ if $\bot \not \in \Gamma(x_t)$. Otherwise, \verb+x+ \emph{may} be an implicit intent, denoted $\emph{implicit}(x)$. 
  %Observe that $\emph{explicit}(x)$ indicates that \verb+x+ must be an explicit intent, while $\emph{implicit}(x)$ means \verb+x+ may be explicit.
  
  \begin{example}\label{ex:data-flow}
Consider again the code  from Figure~\ref{fig:icc-ex}. Here, for  Intent \verb+i+ declared at line 7, we have $
\Gamma(i_t) = \{ \bot \}, \ \   \Gamma(i_a) = \{ {\tt \small action.SEND} \}, \ \ \Gamma(i_d) = \{ {\tt \small text/plain} \}
$
For the intent \verb+n+ at line 13, our  analysis computes $
\Gamma(n_t) = \{ {\tt \small MsgAct} \},   \ \ \Gamma(n_a) = \{ \bot \}, \ \  \Gamma(n_d) = \{ \bot \}
$.
Since $\bot \not \in \Gamma(n_t)$, we conclude $\emph{explicit}(n)$. On the other hand, \verb+i+ is identified to be implicit.
\end{example}
 
 \subsubsection{ICCG Construction}
 
 
\begin{figure}
\[
\begin{array}{c}
\irule{
{\rm must\_alias}(y, x) 
}{
\Gamma \vdash {\rm newval}(y, x, s): [ y_t \mapsto \{ s \} ]
} \\ \ \\ 
 \irule{
{\rm may\_alias}(y, x), \ \ \neg {\rm must\_alias}(y, x)  
}
{\Gamma \vdash {\rm newval}(y,x, s): [ y_t \mapsto  (\Gamma(y) \cup \{s \}) ]  }\\ \ \\
\irule{
\neg {\rm may\_alias}(y, x)
} 
{\Gamma \vdash {\rm newval}(y,x, s): [y_t \mapsto \Gamma(y)]
}  \\ \ \\
\irule{
\begin{array}{c}
\Gamma \vdash {\rm newval}(x_i,x, s): \Gamma_i  \ \ (x_i \in {\rm dom}(\Gamma))\\ 
\end{array}
}{
\Gamma \vdash {\tt x.setComponent(s)}: \bigcup_i \Gamma_i 
} 

\end{array}
\]
\vspace{-0.2in}
\caption{Transfer function for {\tt setComponent}}\label{fig:transfer-func}
\vspace{-0.1in}
\end{figure}

\begin{figure}[!t]
\[
\begin{array}{ccc}
\irule{
x \hookrightarrow o, \ y \hookrightarrow o
}
{{\rm may\_alias}(x, y)} &
\irule{
\begin{array}{c}
x \hookrightarrow o, \neg \exists o'. x \hookrightarrow o' \\
y \hookrightarrow o, \neg \exists o'. y \hookrightarrow o'
\end{array}
}
{{\rm must\_alias}(x, y)} &
\irule{
}
{{\rm must\_alias}(x, x)}
\end{array}
\]
\vspace{-0.2in}
\caption{May and must aliasing relations}\label{fig:alias}
\vspace{-0.1in}
\end{figure}
 
 We now describe  ICCG construction using the results of the intent analysis. In what follows, 
we write $\emph{icc\_site}(m, i)$ to denote that method $m$ contains an ICC site with intent $i$,
and we write $P \leadstostar m$ to indicate that component $P$ has a life-cycle method $m'$ and $m' \leadstostar m$ 
Finally, the predicate $\emph{intent\_filter}(P, A, D)$ means that component $P$ declares an intent filter with action $A$ and data type $D$. This information is extracted from the application's manifest file.


 
 Figure~\ref{fig:iccg-construct} shows the ICCG construction rules.  The first rule (Explicit) considers ICC sites in method $m$ where  intent $i$ has its target component explicitly specified (i.e., $Q \in \Gamma(i_t)$ and $Q \neq \bot$). In this case, if method $m$ is reachable from component $P$, we add an edge from component $P$ to  $Q$ in the ICCG. Furthermore, if $A \in \Gamma(i_a)$ and $D \in \Gamma(i_d)$, the edge from $P$ to $Q$ has action label $A$ and data type label $D$.
 
 The second rule in Figure~\ref{fig:iccg-construct} applies to ICC sites  where  intent $i$ may be implicit. If  $A \in \Gamma(i_a)$ and $D \in \Gamma(i_d)$, we need to determine all components $Q$ that declare  intent filters with action $A$ and data type $D$. Hence,  we add an edge from component $P$ to $Q$ if $\emph{intent\_filter}(Q, A, D)$ is true. 
 
   \begin{example}
Consider again the code  from Figure~\ref{fig:icc-ex} and the data-flow facts $\Gamma$ computed in Example~\ref{ex:data-flow}. Using the Implicit rule for the ICC site in method {\tt foo} with intent {\tt i},  we infer the edge
$
{\small 
( { \tt MainAct}, {\tt MsgAct}, {\tt action.SEND}, {\tt text/plain})
}
$.
Using the Explicit rule for ICC site in method {\tt bar} with intent {\tt n}, we add the edge
$
{\small 
( { \tt MainAct}, {\tt MsgAct}, \bot, \bot)
}
$.
\end{example}
 
 
  

 \begin{figure}
 \[
 \begin{array}{c}
 \irule{
 \begin{array}{c}
 \emph{icc\_site}(m, i), \emph{explicit}(i), \ P \leadstostar m \\
 Q \in \Gamma(i_t), A \in \Gamma(i_a), D \in \Gamma(i_d) \\
 \end{array}
 } {
 (P, Q, A, D) \in E
 } \ \ \ \   {\rm (Explicit)} \\ \ \\
 \irule{
 \begin{array}{c}
  \emph{icc\_site}(m, i), \emph{implicit}(i), \ P \leadstostar m \\
  A \in \Gamma(i_a), D \in \Gamma(i_d) \\
  \emph{intent\_filter}(Q, A, D) 
 \end{array}
 } 
 {(P, Q, A, D) \in E } \ \ {\rm (Implicit)} 
 \end{array}
 \]
\vspace{-.2in}
 \caption{ICCG construction rules}\label{fig:iccg-construct}
 \vspace{-0.1in}
 \end{figure}
 
 
 \begin{comment}
 
 

 {\bf (* Question: What happens if I set both target and action of an intent, but the the target does not perform that action? 
      What are the semantics of intents that have both actions and targets specified? Not sure, please check. --Isil*)}
// -------------------------------------------

In this section, we describe how to compute the predicate \texttt{iccg}.

In Android, intents can be used for explicit or implicit
inter-component communication. An explicit \texttt{Intent} specifies
the component that it should be delivered to. In contrast, an implicit
\texttt{Intent} does not specify the component to which the intent is
to be delivered. Instead, it specifies the type of action the
recipient component needs to perform and the data on which the action
is to be performed. Depending on the type of action and data, the
intent is delivered to a set of suitable components.

Table~\ref{table:intentapis} shows the list of Android API methods in
the \texttt{android.content.Intent} class for specifying the recipient
component, action, and data attributes of intents.

\begin{table}
  \centering
  \begin{tabular}{|c||l|}
      \hline
    Target  &
      \begin{tabular}{l}
                   \texttt{ setComponent(ComponentName)}\\
                   \texttt{ setClassName(Context, String)}\\
                   \texttt{ setClassName(String, String)}\\
                   \texttt{ setClass(Context, Class)}\\ 
        \end{tabular} \\
    \hline
    Action &
		  \begin{tabular}{l}
                   \texttt{ setAction(String)}
                  \end{tabular} \\
    \hline
    Data type &
		\begin{tabular}{l}
                   \texttt{ setType(String)}\\
                   \texttt{ setData(URI)}\\
                   \texttt{ setDataAndType(URI,String)}\\
                   \end{tabular} \\
    \hline
  \end{tabular}
\caption{Android API for attributes of explicit and implicit intents}
\label{table:intentapis}
\end{table}


ICCG construction involves two steps. In the first step, Apposcopy
computes the following four relations by going through each statement
of the app:
\[
setClass, setType, setAction \subseteq V \times V
\]
\[
site \subseteq M \times V
\]
where $V$ and $M$ are the sets of all program variables and methods,
respectively.


%\begin{itemize}

Figure~\ref{fig:iccg-step1} shows the rules to compute four types of
relations are computed from method invocation expressions of the
program.  The invocation expressions in Rule 1-3 invoke a method from
Table~\ref{table:intentapis} that sets one of the three types of
attributes (e.g., \texttt{Action}, \texttt{Data}) of an intent. The
invocation expression in Rule 4 invokes a ICC method from
Table~\ref{table:APIs} that starts a new \texttt{Activity}.  For each
of invocation expression in the program that are of the same forms as
shown in Figure~\ref{fig:iccg-step1}, the analysis updates the
corresponding relation. For example, as per Rule 1 the analysis adds
the pair $(i,cname)$ to relation $setComp$ corresponding to expression
\texttt{i.setClassName(ctxt, cname)}.  To avoid redundancies, we
show usage of only a set of representative methods from
Table~\ref{table:intentapis} and Table~\ref{table:APIs}; other methods
are handled similarly.



\begin{figure*}
\begin{tabular}{llr}
$\llbracket \texttt{i.setClassName(ctxt, cname)}\rrbracket$ & $(i,cname) \subseteq setComp$ & (1)\\
$\llbracket \texttt{i.setAction(act)}\rrbracket$ & $(i,act) \subseteq setAction$ & (2)\\
$\llbracket \texttt{i.setType(t)}\rrbracket$ & $(i,t) \subseteq setType$ & (3) \\
$\llbracket \texttt{ctxt.startActivity(i)}\rrbracket$ & if the expression is inside method \texttt{m} then $(m,i) \subseteq site$ & (4)\\
\end{tabular}
\caption{What do we call these? Abstract semantics?}
\label{fig:iccg-step1}
\end{figure*}


In the second step, following three
relations are computed using the rules in Figure~\ref{fig:iccgrules}.
\[
target \subseteq M \times C
\]
\[
action \subseteq M \times A
\]
\[
datatype \subseteq M \times D
\]

The rules in Figure~\ref{fig:iccgrules} use three relations
\begin{itemize}
\item $compObj \subseteq O \times C$ is a set of pairs 
$(o,c)$ such that abstract object $o$ represents an
heap object of type component $c$.

\item $actObj \subseteq O \times A$ is a set of pairs 
$(o,a)$ such that abstract object $o$ represents
action $a$.

\item $typeObj \subseteq O \times D$ is a set of pairs 
$(o,t)$ such that abstract object $o$ represents 
type $t$.
\end{itemize}

Explain! Explain!

\begin{figure}
\begin{center}
\[
\begin{array}{cr}
\irule{
    i_1 \hookrightarrow o, \ i_2 \hookrightarrow o
}
{
  \emph{alias}(i_1,i_2)
} &  ({\rm Alias}) \\ \ \\
\irule{
  \begin{array}{c}
    \emph{setComp}(i_1,k), \ site(m,i_2) \ alias(i_1,i_2) \\ k \hookrightarrow o, \ compObj(o,comp)
  \end{array}
}
{
  \emph{target}(m,comp)
} &  ({\rm SetClass}) \\ \ \\
%\irule{
% \emph{setClassName}(i,n),  \ i \hookrightarrow o_i, \ compname(o_c,n)
%}
%{
%  \emph{target}(o_i,o_c)
%} &  ({\rm SetClassName}) \\ \ \\
\irule{
  \begin{array}{c}
    \emph{setAction}(i_1,a), \ site(m,i_2) \ alias(i_1,i_2) \\ a \hookrightarrow o, \ actionObj(o,act)
  \end{array}
}
{
  \emph{action}(m,act)
} &  ({\rm SetAction}) \\ \ \\
\irule{
  \begin{array}{c}
    \emph{setType}(i_1,t),  \ site(m,i_2) \ alias(i_1,i_2) \\ t \hookrightarrow o \ typeObj(o,type)
  \end{array}
}
{
  \emph{datatype}(m,type)
} &  ({\rm SetType})\end{array}
\]
\end{center}
\caption{Rules of ICCG construction.}
\label{fig:iccgrules}
\end{figure}

Using relations $target$, $action$, and $datatype$, we can compute the
predicate \texttt{icc} as follows.

\begin{definition}
\label{def:iccg}
The predicate \verb+icc(P,Q,A,D)+ is true iff (i) $m_1$ is a life-cycle method defined in component
  \verb+P+, (ii) $m_1 \leadstostar m_2$, (iii) predicates target($m_2$,Q), action($m_2$,A) 
and datatype($m_2$,D) are true.
\end{definition}

{\bf (* all content is in. need to explain)}

ICCG is a graphical representation of the predicate \texttt{icc}.

\begin{definition}
Inter-Component Call Graph (ICCG) is a graph in which the vertices
represent components of an app. There is an edge from a node
representing component \texttt{P} to a node representing component
\texttt{Q} and the edge is labeled by a pair Action A and Data data if
the predicate \texttt{icc(P, Q, A, D)} is true.
\end{definition}

\end{comment}
