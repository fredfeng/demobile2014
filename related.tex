
\section{Related Work}\label{sec:related}

{\bf \emph{Taint analysis.}}
Both \emph{dynamic} and \emph{static} taint analyses have been  proposed for tracking information-flow  in mobile applications. For example, TaintDroid~\cite{taint-droid} and VetDroid~\cite{ZhangCCS13} are dynamic taint analyses that track information flow by instrumenting  the Dalvik VM, and examples of static taint analyses  include
~\cite{flow-droid,scan-droid,android-leaks,EgeleKKV11}. While Apposcopy  employs static taint analysis as one of its components, we observe that 
not every application that leaks sensitive data is malicious --- in fact, many benign apps 
transmit sensitive data  for performing their required functionality. Thus, taint analyses on their own are not sufficient for automatically  differentiating malicious apps from benign apps, and we propose to combine taint analysis with high-level malware signatures to effectively identify malicious code.
\vspace{0.05in}

{\bf \emph{Signature-based malware detection.}}
Many techniques for identifying existing malware are signature-based, meaning that they look for patterns identifying a certain malware family. In its simplest form, 
these patterns are sequences of bytes or instructions~\cite{Griffin09}. Since such syntactic patterns can be  defeated by semantics-preserving transformations, 
previous work  has  considered \emph{semantics-aware} malware detection~\cite{ChristodorescuJSSB05}. Similar to~\cite{ChristodorescuJSSB05}, Apposcopy detects malware based on their semantic rather than syntactic characteristics. However, our signatures are much higher-level compared to the templatized instruction sequences used in~\cite{ChristodorescuJSSB05} and allow directly specifying control- and data-flow properties of Android applications. Furthermore, the underlying signature matching techniques are also very different. 

A popular signature-based malware detection technique for Android is the Kirin tool~\cite{EnckOM09}. The malware signatures in Kirin specify dangerous combinations of Android permissions, and Kirin decides if an  application matches a  signature by analyzing its manifest file. As demonstrated in our experimental evaluation, Kirin yields many more false positives and false negatives compared to Apposcopy.
%However, since many benign apps may also require such permissions to perform their advertised functionality, Kirin may mistakenly classify benign apps as %malware.


Another related approach is the \emph{behavioral detection} technique described in~\cite{BoseHSP08}. In that approach, one specifies common malware behavior using temporal logic formulas. However, a key difference between Apposcopy and behavioral detection is that our techniques are purely static, while~\cite{BoseHSP08} requires monitoring behavior of the application at run-time.

The DroidRanger tool~\cite{Zhou12} uses \emph{permission-based behavioral footprint} to detect instances of known malware families. Behavioral footprints include characteristic malware features, such as listening to certain system events, calling suspicious APIs, and containing  hard-coded strings. While these behavioral footprints can be viewed as high-level malware signatures, they differ from those of Apposcopy in several ways: Behavioral footprints neither refer to information flow properties of an application nor do they express  control flow dependencies between different components. Furthermore,  behavioral footprints  include hard-coded string values, which are easy to obfuscate by malware writers. 
\vspace{0.05in}

{\bf \emph{Zero-day malware detection.}} A known limitation of signature-based approaches, including Apposcopy,  is that they can only detect instances of known malware families. In contrast, zero-day malware detectors try to uncover previously unknown malware families. For example,  RiskRanker~\cite{risk-ranker} performs several kinds of risk analyses to rank Android applications as  high-, medium-, or low-risk.  These risk analyses include techniques to identify  suspicious code that exploits platform-level vulnerabilities or  sends private data without being triggered by user events. In addition to 
identifying instances of known malware, DroidRanger~\cite{Zhou12} also tries to uncover zero-day malware by performing heuristic-based filtering to identify certain ``inherently suspicious" behaviors.

Many recent malware detectors, such as~\cite{drebin,GasconYAR13,Chak13,PengCCS12,aafer2013droidapiminer}, use machine learning  to detect zero-day malware. For example, Drebin~\cite{drebin} performs light-weight static analysis to extract features, such as permissions and API calls,  and  trains an SVM  to find a hyperplane  separating benign apps from malware. 
%A related approach is MAST~\cite{Chak13}, which uses a statistical method, known as multiple correspondence analysis,  to rank applications according to their risk levels. 
The DroidAPIMiner tool~\cite{aafer2013droidapiminer}  also considers API features  and uses machine learning to automatically classify an application as malicious or benign.  While learning-based approaches are powerful for detecting unknown malware, their precision relies on representative training sets.  


We believe all of these zero-day malware detection techniques  are complementary to Apposcopy: While Apposcopy can identify instances of known malware families with few false alarms,  zero-day malware detectors can help uncover new malware families, albeit at the cost of more false positives or more involvement from a security auditor.




{\bf \emph{Static analysis for malware detection.} }
Static analysis and model checking  have been used to detect security vulnerabilities for a long time.
%RESIN\cite{Yip09} designs a new language runtime to prevent security vulnerabilities by data flow assertions specified by programmer in application-level %for web applications. 
In the context of mobile malware, the SAAF tool~\cite{Hoff13} uses  program slicing to identify suspicious method arguments, such as certain URLs or phone numbers. The work described in~\cite{EnckOMC11} performs various  static analyses, including taint analysis, to better understand smartphone application security in over 1,000 popular Android apps. One of the interesting conclusions from this study is that, while many apps misuse privacy-sensitive information, few of these apps can be classified as malware. 





The Pegasus system~\cite{pegasus} focuses on  malware that can be identified by the order in which certain permissions and APIs are used. The user writes specifications of expected app behavior using temporal logic formulas, and  Pegasus model checks these specifications against an abstraction called the \emph{permission event graph (PEG)}. Pegasus differs from our approach in that (i) it targets a different class of malware, and (ii) PEG abstracts the relationship between the Android system and the application, while ICCG abstracts  the relationship between different components in the application.


 Some recent papers  address the detection of re-packaged apps which often inject  adware or malicious 
 features into  legitimate apps~\cite{droid-moss,piggyback,andarwin,dnadroid}. While some repackaged apps may contain malware, these techniques mainly focus on clone rather than malware detection.
\vspace{0.05in}


{\bf \emph{Analysis of ICC.}}  ComDroid~\cite{comdroid} analyzes ICC of Android apps to expose  security vulnerabilities, such as intent spoofing or unauthorized intent receipt.   CHEX~\cite{LuLW12} performs static analysis to identify app entry points and uses this information to detect component hijacking vulnerabilities. In contrast to Apposcopy, ComDroid and CHEX are meant to be used by developers to identify  security vulnerabilities in their own applications.

Epicc~\cite{epicc} also addresses ICC in Android and proposes a static analysis for inferring  \emph{ICC specifications}. These specifications include ICC entry and exit points, information about the action, data and category components of intents used for ICC, as well as Intent key/value types. While our ICCG encodes similar information to the  specifications inferred by Epicc, we show that the ICCG is a useful abstraction for specifying and identifying Android malware.



\begin{comment}
/****Original version is commented out by YU FENG******/
In this section, we survey related work  and explain how they differ from Apposcopy.  \vspace{0.05in}

{\bf \emph{Taint Analysis.}}
Both \emph{dynamic} and \emph{static} taint analyses have been  proposed for tracking information-flow  in Android. For example, TaintDroid~\cite{taint-droid} is a dynamic taint analysis which tracks information flow by instrumenting  the Dalvik VM. Examples of static taint analyses for Android  include~
~\cite{scan-droid,android-leaks}. While Apposcopy  employs static taint analysis as one of its components, we observe that 
not every application that leaks sensitive data is malicious --- in fact, many benign apps 
transmit sensitive data  for performing their required functionality. As a result, taint analyses on their own are not sufficient for automatically  differentiating malicious apps from benign apps, and we propose to combine taint analysis with high-level malware signatures to effectively identify malicious code.
\vspace{0.05in}

{\bf \emph{Malware detection.} }
Many techniques for identifying malware are signature-based, meaning that they look for patterns identifying a certain malware family. In its simplest form, 
these patterns are sequences of bytes or instructions. Since such syntactic patterns can be easily defeated by simple transformations that preserve program semantics, previous work  has also considered \emph{sementics-aware} malware detection~\cite{ChristodorescuJSSB05}. Similar to~\cite{ChristodorescuJSSB05}, Apposcopy detects malware based on their semantic rather than syntactic characteristics. However, our signatures are much higher-level compared to the templatized instruction sequences used in~\cite{ChristodorescuJSSB05} and allow directly specifying control- and data-flow properties of Android applications in a declarative manner. Furthermore, the underlying signature matching techniques are also very different. 
Another related approach is the \emph{behavioral detection} technique described in~\cite{BoseHSP08}. In that approach, one specifies common malware behavior using temporal logic formulas. However, a key difference between Apposcopy and behavioral detection is that our techniques are purely static, while~\cite{BoseHSP08} requires monitoring behavior of the application at run-time.
 
A known limitation of signature-based approaches  is that they can only detect malware of known families. The RiskRanker system described in~\cite{risk-ranker} attempts to uncover \emph{zero-day malware} that has not been previously identified. RiskRanker performs several kinds of risk analyses to rank Android applications as  high-, medium-, or low-risk.  These risk analyses include techniques to identify  suspicious code that exploits platform-level vulnerabilities or code that sends private data without being triggered by user events. Another related approach is DroidAPIMiner~\cite{aafer2013droidapiminer} which considers API features of applications and uses machine learning to automatically classify an application as malicious or benign. We believe  these approaches and Apposcopy  are quite complementary: Apposcopy can identify instances of known malware families with few false alarms, while these approaches can help uncover new malware families.

The Pegasus system~\cite{pegasus} focuses on  malware that can be identified by the order in which certain permissions and APIs are used. The user writes specifications of expected app behavior using temporal logic formulas, and the Pegasus system model checks these specifications against an abstraction called the \emph{permission event graph (PEG)} of the Android application. Pegasus differs from our approach in that (i) it targets a different class of malware than Apposcopy, and (ii) PEG abstracts the relationship between the Android system and the application, while ICCG abstracts  the relationship between different components in the application.


 Recent work  focuses on detecting re-packaged apps which often inject  adware or malicious 
 features into  legitimate apps~\cite{droid-moss,piggyback,andarwin,dnadroid}. While some repackaged apps may  contain malware, these techniques mainly focus on clone --rather than malware-- detection.
\vspace{0.05in}

{\bf \emph{Analysis of ICC.}} ComDroid~\cite{comdroid} analyzes ICC of Android applications to expose potential security vulnerabilities, such as intent spoofing or unauthorized intent receipt, arising from ICC. In contrast to our approach which focuses on identifying malicious apps, ComDroid is meant to be used by developers to identify potential security vulnerabilities in their own applications. Epicc~\cite{epicc} also addresses ICC in Android and proposes a static analysis for inferring  \emph{ICC specifications}. These specifications include ICC entry and exit points, information about the action, data and category components of intents used for ICC, as well as Intent key/value types. While our ICCG encodes similar information to the  specifications inferred by Epicc, we show that the ICCG is a useful abstraction for specifying semantic characteristics of Android malware.% families.
% and that it strikes a good balance between precision 
%and scalability in the context of malware detection.

\end{comment}


