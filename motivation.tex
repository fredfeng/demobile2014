\section{An Example Illustrating our approach}\label{sec:example}

%Figure~\ref{fig:overview} shows the overview of our technique:

\begin{comment}
\begin{figure*}
\vspace{-0.1in}
\begin{center}
\includegraphics[scale=0.35]{iccg-ex-new.png}
\end{center}
\vspace{-0.2in}
\caption{Partial ICCG for an instance of the GoldDream malware family}\label{fig:iccg-ex}
\end{figure*}
\end{comment}

We illustrate Apposcopy's basic approach using a simplified version of the GoldDream malware family.
\begin{comment}
As described in Jiang's security report~\cite{JiangDream}, the key characteristic of members of this family
is that they register a receiver for certain system events such as SMS messages or outgoing phone calls. 
When these events trigger the execution of code in the receiver, the malware then starts a background service 
for sending private user information, such as the phone's unique IMEI number and subscriber id, to a remote server.
\end{comment}

\subsection{GoldDream  Signature in Apposcopy}\label{sec:ex-spec}


To detect a  sample of GoldDream malware, an analyst first writes a signature of this malware family
in our Datalog-based language. In this case, the behavior of  GoldDream  is captured by the specification  in Figure~\ref{fig:golddream-spec}.
Here, lines 1-2 introduce a new user-defined  predicate \verb+GDEvent(x)+ which describes the events that the {\tt GoldDream} malware  listens for.  In particular, \verb+GDEvent(x)+ evaluates to true when \verb+x+ is either  \verb+SMS_RECEIVED+ or \verb+NEW_OUTGOING_CALL+ but  to false otherwise.

Using this predicate, lines 3-7 describe the  signature of the GoldDream malware family. The signature uses three kinds of predicates provided by Apposcopy: 

{\bf Component type predicates}, such as \verb+receiver(r)+ and \verb+service(s)+, 
%are examples of component type predicates and 
specify  that \verb+r+ and \verb+s+  are \verb+BroadcastReceiver+ and \verb+Service+ components in the Android framework. 


{\bf Control-flow predicates}, such as \verb+icc+, which describes inter-component 
communication. \verb+icc(SYSTEM, r, e, _)+ expresses that the Android system invokes component \verb+r+ when system event \verb+e+ happens, and \verb+icc*(r, s)+ means that component \verb+r+ transitively invokes component~\verb+s+. 


{\bf Data-flow predicates}, such as \verb+flow(x, so, y, si)+, express that the application contains a flow from  source \verb+so+ in component \verb+x+ to a sink \verb+si+ in component \verb+y+. Hence, lines 6-7 state that component \verb+s+  sends  the device and  subscriber id of the phone over the Internet.   


Therefore, according to the signature in Figure~\ref{fig:golddream-spec}, an application $A$ belongs to the GoldDream malware family if (i) $A$ contains a broadcast receiver that listens  for system events \verb+SMS_RECEIVED+ or \verb+NEW_OUTGOING_CALL+ (lines 3, 4), and (ii) this broadcast receiver starts a service which then leaks the device id and subscriber id over the Internet (lines 5-7). 
%Observe that variables such as \verb+r+, \verb+s+ used in this definition are implicitly existentially quantified. Also, note that the commas in the %definition correspond to logical conjunction; hence, Apposcopy will only conclude that an app belongs to the GoldDream malware family if all predicates  in %the specification are satisfied.

%% declare events that GoldDream listens for.
% actual signature for GoldDream malware.
\begin{figure}
%\vspace{-0.15in}
\small\begin{verbatim}
1. GDEvent(SMS_RECEIVED).
2. GDEvent(NEW_OUTGOING_CALL).
3. GoldDream :-  receiver(r), 
4.               icc(SYSTEM, r, e, _), GDEvent(e), 
5.               service(s), icc*(r, s), 
6.               flow(s, DeviceId, s, Internet), 
7.               flow(s, SubscriberId, s, Internet).
\end{verbatim}
\vspace{-0.2in}
\caption{GoldDream signature (simplified)}\label{fig:golddream-spec}
\vspace{-0.21in}
\end{figure}

\subsection{GoldDream Malware Detection}

Given an Android application $A$ and  malware signature $S$, Apposcopy performs static analysis to decide if app $A$ matches signature $S$. Apposcopy's  static analysis  has two important ingredients:
(i) \emph{construction of the ICCG},  which allows determining the truth values of control-flow predicates used in the signature, and (ii) \emph{static taint analysis}, which is used to decide the truth values of data-flow predicates. In particular, the ICCG is a graph where nodes are Android components and edges denote inter-component call relations. On the other hand, Apposcopy's taint analysis identifies which designated sources(i.e., sensitive data) can flow to which designated sinks(e.g., Internet, SMS message, etc.). 
Since Apposcopy's static analyses are both sound and sufficiently precise, it can detect whether an application matches the signature from Figure~\ref{fig:golddream-spec} with very few false positives.


\begin{comment}
Figure~\ref{fig:iccg-ex} shows a partial ICCG for an instance of the GoldDream malware family. Nodes in the ICCG correspond to components, and node labels denote component names. The shapes of the nodes indicate component types: Rectangles denote broadcast receivers, ellipses indicate activities, and polygons are services.
An ICCG edge from one node  $A$ to another node  $B$ means that component $A$ starts component $B$, for example, by calling the \verb+startActivity+ method of the Android SDK. The edges in the ICCG may also be labeled with additional information, such as system events. 
%We discuss edge labels in more detail in Section~\ref{sec:iccg}.

Going back to the specification  from Section~\ref{sec:ex-spec}, the ICCG shown in Figure~\ref{fig:iccg-ex} matches the sub-query
\vspace{-0.05in}
\[ 
\small
\begin{array}{l}
{\tt receiver(r), \ } 
{\tt icc(SYSTEM, r, e, \_), GDEvent(e),}  \\
{\tt service(s), icc*(r, s)}
\end{array}
\vspace{-0.05in}
\]
because (i) there exists a node in the ICCG representing a receiver component (namely, \verb+zjReceiver+), (ii) there is an edge from the node representing the Android system to \verb+zjReceiver+ labeled with \verb+SMS_RECEIVED+, (iii) \verb+zjReceiver+ has an outgoing edge to a service component called \verb+zjService+. 
%Hence, as this example illustrates, the ICCG is useful for deciding control-flow queries about a given application.

Next, to decide data-flow queries, Apposcopy performs  taint analysis tracking flows from \emph{sources} to
\emph{sinks}. Here, sources represent sensitive data and sinks represent operations that may leak data. 

\small
\vspace{-0.05in}
\begin{verbatim}
com.sjgo.client.zjService:
    $getDeviceId -> !INTERNET
    $getSubscriberId -> !INTERNET
    ...
\end{verbatim}
%    $getSimSerialNumber -> !INTERNET
%    $getDeviceId -> !sendTextMessage
%    $getSubscriberId -> !sendTextMessage   

% $getSimSerialNumber -> !sendTextMessage
%    $RELEASE -> !INTERNET


\normalsize

Here, Apposcopy has identified source-sink flows in component \verb+zjService+.
For example, the first three lines under \verb+zjService+ indicate that it sends the phone's serial number, device id, and 
subscriber id over the Internet. This behaviro matches line 6 and 7 of the GoldDream malware signature.

\vspace{-0.05in}
\small
\begin{verbatim}
flow(s, DeviceId, s, Internet),  
flow(s, SubscriberId, s, Internet)
\end{verbatim}
\normalsize

\vspace{-0.05in}
\noindent
where \verb+s+ is a service. 

Since the taint analysis reveals that {\tt zjService} leaks  the device and subscriber id to the Internet, thus Apposcopy concludes  this application is GoldDream malware. 

Observe that, although there are  other source-sink flows in this example (such as from {\tt DeviceId} to {\tt WebView} in {\tt AdActivity}),  these other flows do not necessarily correspond to malicious behavior.
\end{comment}
