\subsection{Taint analysis}
\label{sec:taint}
We now describe Apposcopy's  taint analysis for 
answering data-flow queries. We first discuss how 
we annotate sources and sinks  and then 
describe our taint propagation rules.


%In this section, first we will describe three types of annotations in our tainted analysis
%and then go over their propagation rules.




\subsubsection{Annotations}\label{sec:annotations}
Apposcopy provides three types of annotations called \emph{source}, \emph{sink}, and \emph{transfer}
annotations. Source annotations are used to mark Android framework methods that 
read sensitive data, and sink annotations indicate methods that  leak  data outside of the device.
In contrast, transfer annotations are used for describing taint flow through Android SDK methods.
In our approach, transfer annotations are necessary because Apposcopy analyzes the source code of Android applications, but \emph{not} the underlying implementation
of the Android platform. Hence, we have manually written annotations for Android SDK methods that propagate taint between parameters and return values.

\begin{example}
Figure~\ref{fig:taintAnnotate} shows representative examples of source, sink, and transfer annotations. All three kinds of specifications are written using the \verb+@Flow+ annotation, and sources and sinks are indicated by 
the special prefixes \verb+$+ and \verb+!+ respectively.  In Figure~\ref{fig:taintAnnotate}, the annotation at line 2 is a source annotation, indicating that \verb+getDeviceId+'s return value is  a taint source. The sink annotation at line 8 indicates that \verb+sendTextMessage+ is a sink for its formal parameter \verb+text+. Lines 11-12 correspond to transfer annotations for the Android  library method \verb+concat+ which is not 
analyzed by Apposcopy. According to line 11, if the \verb+this+ parameter of \verb+concat+ is tainted, then its return value is also tainted. Similarly, the annotation at line 12 states that if parameter \verb+s+ of \verb+concat+ is tainted, then so is the return value.
\end{example}


We emphasize that the source, sink, and transfer annotations  are \emph{not} written by individual users of Apposcopy, 
which already
 comes with a set of built-in  annotations that we have written.  Apposcopy uses the
same pre-defined annotations for analyzing every  Android application.



\subsubsection{Static Taint Analysis}
We describe Apposcopy's taint analysis  using the 
inference rules shown in Figure~\ref{fig:taintedrules}, which define two predicates $\emph{tainted}(o, l)$ and 
${\emph flow(so, si)}$. The predicate $\emph{tainted}$ represents a relation
$
(O: {\rm AbstractObj}, \  L: {\rm SourceLabel})
$
where domain $O$ is the set of all abstract heap objects  and $L$ is the set of all source labels, such as {\tt\$getDeviceId}. If  $\emph{tainted}(o, l)$ is true, this means that any concrete heap objected represented by $o$ may be tainted with $l$.
The predicate $\emph{flow}(so, si)$ represents a relation of type
$
(L: {\rm SourceLabel}, \  L: {\rm SinkLabel})
$.
If  $\emph{flow}(so, si)$ is true, this means that source $so$ may reach sink $si$. Hence, the $\emph{flow}$ predicate is a static over-approximation of the taint flow relation introduced in Definition~\ref{def:taint-flow}.




\begin{figure}[!t]
\begin{center}
{ \small
\begin{verbatim}
1.  //Source annotation in android.telephony.TelephonyManager
2.  @Flow(from="$getDeviceId",to="@return")
3.  String getDeviceId(){ ... } 

7.  //Sink annotation in android.telephony.SmsManager
8.  @Flow(from="text",to="!sendTextMessage")
9.  void sendTextMessage(...,String text,...){ ... } 

10. //Transfer annotation in java.lang.String
11. @Flow(from="this",to="@return")
12. @Flow(from="s",to="@return")
13. String concat(String s){ ... }
\end{verbatim}
}
\end{center}
\vspace{-0.2in}
\caption{Source, Sink and Transfer annotations.}
\label{fig:taintAnnotate}
\vspace{-0.1in}
\end{figure}


All of the rules in Figure~\ref{fig:taintedrules} use the notation $m_i$ to denote the $i$'th parameter of method $m$. For uniformity of presentation, we represent the \verb+this+ pointer as $m_0$ and the  return value as $m_{n+1}$ where $n$ is the number of arguments of $m$.

The first rule labeled Source in Figure~\ref{fig:taintedrules} describes taint introduction. In this rule, we use the notation $\emph{src}(m_i, l)$ to denote that the $i$'th parameter of $m$ is annotated with source label $l$ as described in Section~\ref{sec:annotations}. Hence,  if variable $m_i$ may point to heap object $o$ and $m_i$ is annotated as source $l$, then heap object $o$ also becomes tainted with label $l$. 

The second rule called Transfer performs taint propagation. Here, the predicate $\emph{transfer}(m_i, m_j)$ corresponds to the transfer annotations  from Section~\ref{sec:annotations} and indicates there is a flow from the $i$'th to the $j$'th parameter of Android SDK method $m$. According to this rule, if (i) $m_i$ can flow to $m_j$, (ii) $m_i$ and $m_j$ may point to heap objects $o_1$ and $o_2$ respectively, and (iii) $o_1$ is tainted with label $l$, then $o_2$ also becomes tainted with label $l$.

The third rule labeled Sink defines the $\emph{flow}$ predicate using the $\emph{tainted}$ predicate. Here, the notation $\emph{sink}(m_i, si)$ means that the $i$'th parameter of $m_i$ is passed to some sink labeled $si$.  Hence, according to this rule, if  $m_i$ is passed to sink $si$, and $m_i$ may point to a heap object $o$ that is tainted with label $so$, then there may be a flow from source $so$ to sink $si$.

The taint analysis of Apposcopy consists of applying the rules from Figure~\ref{fig:taintedrules} until a fixed-point is reached. Observe that the rules from Figure~\ref{fig:taintedrules} do not describe transformers for individual  instructions such as  stores because we use the points-to facts computed by a whole-program pointer analysis.  That is, if any variable $v$ in the program  may flow to $m_i$ through a chain of heap reads and writes, we will have $m_i \hookrightarrow o_v$ where $o_v$ is the  location pointed to by $v$.~\footnote{For simplicity of presentation, we assume  every variable is a pointer to a heap object.}






\begin{example}
Consider the code in Figure~\ref{fig:taint-ex} and  annotations from Figure~\ref{fig:taintAnnotate}. Suppose  \verb+x+, \verb+y+, \verb+z+ point to heap objects $o_1, o_2, o_3$ respectively. Since ${\small \verb+src(getDeviceId+}_\emph{return}, {\small \verb+$getDeviceId+})$ and  $y \hookrightarrow o_2$, the Source rule infers  ${\small \verb+tainted+}(o_2, {\small \verb+$getDeviceId+})$.
${\small \verb+transfer+}({\tt concat}_1, {\tt concat}_\emph{return}) $denotes a transfer annotation at line 5 where ${\tt concat}_1 \hookrightarrow o_2$, ${\small  \verb+concat+}_\emph{return} \hookrightarrow o_3$; thus, the Transfer rule infers ${\small \verb+tainted+}(o_3, {\small \verb+$getDeviceId+})$.
Finally, consider the call to method {\small \verb+sendTextMessage+} which has a sink annotation ${\small \verb+sink+}({\small \verb+sendTextMessage+}_3, {\small \verb+!getDeviceId+})$. Since the argument \verb+v+ and \verb+z+ are aliases, we have ${\small \verb+sendTextMessage+}_3 \hookrightarrow o_3$. 
%Now, since we have deduced ${\small \verb+tainted+}(o_3, {\tt \$getDeviceId})$ and since, from the annotation, we have $\emph{sink}(m_3, {\tt !sendTextMessage})$,
Hence, we deduce
$
{\small \verb+flow+}({\small \verb+$getDeviceId+}, {\small \verb+!sendTextMessage+})
$.
\end{example}





\begin{comment}
\begin{figure}
\begin{center}

\begin{verbatim}

---Propagation Rules for tainted analysis----
% declare source label.
% l is the source label in method m.
1. src(m,l).

% declare sink label.
% l is the sink label in method m.
2. sink(m,l).

% declare transfer annotation.
% Tainted variable flows from ith paramter in m1 
% to jth parameter in m2.
3. xfer(m1,i,m2,j).

% flow where src is source and sk is the sink.
4. flow(src,sk).

% declare points-to information
% The ith parameter of method m points-to o.
% i=-1 denotes the return value of m.
5. pt(m,i,o).

%  o is tainted by source lable l.
6. tainted(o,l)  :- pt(m,-1,o), src(m,l). 
7. tainted(o2,l) :- tainted(o1,l), pt(m,i,o1), 
8.                  xfer(m1,i,m2,j), pt(m,j,o2).
9. flows(src,sk) :- tainted(o,src), sink(m,i,sk), 
10.                 pt(m,i,o).
              
-----------------------------------------------              
\end{verbatim}
\end{center}
\caption{Propagation rules of tainted analysis.}
\label{fig:taintedrules}
\end{figure}
\end{comment}

\begin{figure}[!t]
\begin{center}
\[
\begin{array}{cr}
\irule{
 \emph{src}(m_i,l),  \ m_i \hookrightarrow o
}
{
  \emph{tainted}(o,l)
} &  ({\rm Source}) \\ \ \\
\irule {
\begin{array}{c}
 \emph{tainted}(o_1,l), \ m_i \hookrightarrow o_1, \ m_j \hookrightarrow o_2 \\ \emph{transfer}(m_i, m_j)
 \end{array}
}
{ \emph{tainted}(o_2,l) } & ({\rm Transfer}) \\ \ \\
\irule
{\emph{tainted}(o,so), m_i \hookrightarrow o, \emph{sink}(m_i, si)}
{\emph{flow}(so, si)} & ({\rm Sink})
\end{array}
\]
\end{center}
\vspace{-0.2in}
\caption{Rules describing the taint analysis.}
\label{fig:taintedrules}
\vspace{-0.1in}
\end{figure}






