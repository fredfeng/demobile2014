\section{Malware Spec Language}\label{sec:language}

This section describes Apposcopy's malware signature language, which is a Datalog program augmented with 
built-in predicates. For each malware family, the user
defines a unique predicate  that serves as
the signature for this malware family.  The user may also define 
additional helper predicates  used in the signature.
In what follows, we first give some background on Datalog, and then describe the 
 the syntax and semantics of Apposcopy's built-in predicates.

\subsection{Datalog Preliminaries}\label{sec:datalog}


%Datalog is a logic programming language for deductive databases, first formalized by
%Ullman~\cite{Ullman89}. 
A Datalog program  consists of a set of \emph{rules} and a set of \emph{facts}. 
Facts simply declare predicates that evaluate to true. For example, 
{\tt parent("Bill", "Mary")} states that Bill is a parent of Mary. Each  Datalog rule is  a Horn clause defining a predicate as a conjunction of other
predicates. For example, the  rule:

\small
\vspace{-0.06in}
\begin{verbatim}
ancestor(x, y) :- parent(x, z), ancestor(z, y).
\end{verbatim}
\vspace{-0.06in}
\normalsize

\noindent
says that \verb+ancestor(x, y)+ is true if both \verb+parent(x, z)+ and
\verb+ancestor(z, y)+ are true. In addition to variables, predicates can 
also contain constants, 
which are surrounded by double quotes, or
``don't cares",  denoted by underscores.
%Predicates on the
%right hand side of the rules are  allowed to appear negated (e.g., \verb+!parent(x,y)+).




%This represents that $parent(bill,mary)$ is true, where $bill$ and
%$mary$ are two constants in the respective domains over which $parent$
%is defined.

%In the example above, the predicate to the left of the :- symbol is called the head
%predicate. Predicates to the right of the :- symbol are called subgoal predicates.

Datalog predicates naturally represent relations. Specifically, if tuple $(x, y, z)$
is in relation $A$, this means the  predicate $A(x, y, z)$ is
true. In what follows, we write the type of a relation $R \subseteq X \times Y \times \ldots$ as $(s_1 : X, s_2 : Y, \ldots)$, where
$s1$, $s2$, $\ldots$ are descriptive texts for the corresponding domains.

\begin{table}
  \vspace{-0.1in}
  \centering
  \caption{A partial list of ICC-related APIs.} \label{table:ICCAPIs}
  \small\begin{tabular}{|c|l|}
    
    \hline
    
    Activity & 
    \begin{tabular}{l} 
      \texttt{startActivity(Intent)}\\                    
      \texttt{startActivityForResult(Intent,int)}\\
      \texttt{startActivityIfNeeded(Intent,int)}\\
      \texttt{startNextMatchingActivity(Intent)}  
    \end{tabular}\\

    \hline

    Service &
    \begin{tabular}{l}
      \texttt{startService(Intent)}\\
      \texttt{bindService(Intent)}
    \end{tabular}\\

    \hline

    BroadcastReceiver &
    \begin{tabular}{l}
      \texttt{sendBroadcast(Intent)}\\
      \texttt{sendBroadcast(Intent,String)}\\
      \texttt{sendOrderedBroadcast(Intent,String)} \\
    \end{tabular}\\

    \hline

  \end{tabular}
  \vspace{-0.05in}
\end{table}

\subsection{Apposcopy's Built-in Predicates}
\label{sec:preds}
We now describe the syntax and semantics of the four classes of built-in predicates provided by Apposcopy.


\subsubsection{Component type predicates}
Component type predicates in Apposcopy  represent the different kinds of components
provided by the Android framework. An Android application consists of four kinds of components, 
\emph{Activity}, \emph{Service}, \emph{BroadcastReceiver}, and \emph{ContentProvider}.
Activity components form the basis of the user
  interface, and each window of the application is typically controlled by
  an activity. Service components run in the background and remain active even
  if windows are switched. 
  %Services can expose interfaces for communication with other applications.
BroadcastReceiver components react asynchronously to messages
  from other applications. Finally, ContentProvider components store data relevant to the
  application, usually in a database, and allow sharing  data across applications.




Corresponding to each of these Android components, Apposcopy
provides  pre-defined predicates called
\verb+service(C)+, \verb+activity(C)+, \verb+receiver(C)+, and  
\verb+contentprovider(C)+
which represent the type of component \verb+C+.  For example, \verb+activity(C)+
is true if \verb+C+ is an \texttt{Activity}. Each of these four predicates
correspond to relations of type $(comp : C)$ where domain $C$ is the set of all components in
the application.



\subsubsection{Predicate {\tt icc}}

%\begin{comment}


\begin{table}
\vspace{-0.1in}
  \centering
  \caption{A partial list of life-cycle APIs.} \label{table:LCAPIs}
  \small\begin{tabular}{|c|l|}
    
    \hline
    
    Activity & 
    \begin{tabular}{l} 
      \texttt{onCreate(Bundle)},  
      \texttt{onRestart()}, \\ 
      \texttt{onStart()}, 
      \texttt{onResume()},  \\
      \texttt{onPause()}, 
      \texttt{onStop()},   
      \texttt{onDestroy()}
    \end{tabular}\\

    \hline

    Service &
    \begin{tabular}{l}
      \texttt{onCreate()}, 
      \texttt{onBind(Intent)}, \\  
      \texttt{onStartCommand(Intent, int, int)},\\  
      \texttt{onDestroy()}\\  
    \end{tabular}\\

    \hline

    BroadcastReceiver &
    \begin{tabular}{l}
      \texttt{onReceive(Context, Intent)}\\
    \end{tabular}\\
    \hline

  \end{tabular}
  \vspace{-0.1in}
\end{table}
      

%\end{comment}
A key idea behind inter-component communication (ICC) in Android is
that of Intents, which are messages passed between
components.  Intents are used to start Activities; start, stop, and
bind Services; and broadcast information to Broadcast
Receivers. Table~\ref{table:ICCAPIs} shows a list of Android APIs that
are used in ICC. We refer to those methods, all of
which take Intent objects as arguments, as \emph{ICC
  method}s.  We
use the term \emph{ICC site} to represent a statement 
that invokes one of the ICC methods listed in
Table~\ref{table:ICCAPIs}. When component $A$ initiates ICC with
 component $B$, the Android system will eventually call
\emph{life-cycle} methods associated with component $B$. The
life-cycle methods are  shown in Table~\ref{table:LCAPIs}.


While an Intent object passed to ICC methods can contain many different types of information, Apposcopy's
signature  language takes into account only two kinds: action and data. \emph{Action} is a string that
represents the type of action that the receiving component needs to
perform, and \emph{data} specifies the type of data that the component needs to operate on. 
%is a Universal Resource Identifier (URI) that
%identifies the data that the receiving component needs to operate on.
For example, a component for editing images will receive intents with corresponding
action  \verb+EDIT+ and data type  \verb+image/*+.



%and data values
%as ``ACTION\_VIEW'' and ``tel:123'' may display the phone dialer with
%the  number 123 filled in. 

Apposcopy's built-in \verb+icc+ predicate  represents inter-component
communication in Android and corresponds to a
relation of  type
$
(source:C, target:C, action:A, data:D)
$
where domain $C$ is the set of all components in the application, and  $A$ and $D$  
are the sets of all Action and
Data values defined by the Android system. 
Since not all intents are required to have action and data values,
domains $A$ and $D$ also include a special element $\bot$, which indicates no value.
Intuitively, if predicate \verb+icc(P,Q,A,D)+ is true, this means that component \verb+P+ passes an intent to  \verb+Q+ through
invocation of an ICC method, and \verb+A+ and \verb+D+ represent the action 
and data strings of the intent. 
To formally state the semantics of the \verb+icc+ predicate, we first define 
targets of ICC sites:



\begin{definition}
\vspace{-0.05in}
The \emph{target of an ICC site}, \texttt{m(i,$\ldots$)}, is the set of
components that will receive the intent stored in variable \texttt{i}
 in some execution of the application.
\end{definition}

\begin{definition} 
\label{def:leadsto}
\vspace{-0.05in}
We write $m_1 \leadsto m_2$ iff method $m_1$ directly calls 
$m_2$ in some execution of the program. We define $\leadstostar$ to be the reflexive 
transitive closure of  $\leadsto$.
\vspace{-0.05in}
\end{definition} 

In other words, $m_1 \leadstostar m_2$ if $m_1$  transitively calls $m_2$.
We  now  define the semantics of the \verb+icc+ predicate as follows:

\begin{definition}
\vspace{-0.05in}
The predicate \verb+icc(P,Q,A,D)+ is true iff (i) $m_1$ is a life-cycle method defined in component
  \verb+P+, (ii) $m_1 \leadstostar m_2$, (iii) $m_2$ contains an ICC site whose
  target is component \verb+Q+, and (iv) the action and data values of the intent
are \verb+A+ and \verb+D+, respectively.
\vspace{-0.05in}
\end{definition}


\begin{figure}
\begin{center}
\small
\begin{verbatim}
1. public class MainAct extends Activity {
2.    protected void onCreate(Bundle b) {
3.        foo();
4.        bar();
5.    }
6.    void foo() {
7.       Intent i = new Intent();
8.       i.setAction(android.intent.action.SEND);
9.       i.setType("text/plain");
10.      startActivity(i);  
11.   }
12.    void bar() {
13.       Intent n = new Intent();
14.       n.setClass(MsgAct.class);
15.       startActivity(n);  
16.   }
17. }
18. public class MsgAct extends Activity { ... }
\end{verbatim}
\end{center}
\vspace{-0.2in}
\caption{ICC example.}
\label{fig:icc-ex}
\end{figure}

\begin{figure}
\begin{center}
\small
\begin{verbatim}
 <activity android:name="MsgAct">
   <intent-filter
     <action name="android.intent.action.SEND" />
     <data mimeType="text/plain" />
  </intent-filter>
 </activity>
\end{verbatim}
\end{center}
\vspace{-0.2in}
\caption{A snippet of AndroidManifest.xml}
\label{fig:icc-ex-manifest}
\vspace{-0.1in}
\end{figure}


\begin{example}
Consider the code  in Figure~\ref{fig:icc-ex}, which
defines two activities, {\tt \small MainAct} and
{\tt \small MsgAct}. The {\tt \small onCreate} life-cycle method of
{\tt \small MainAct} calls \verb+foo+ which initiates ICC at line 10 by
calling {\tt \small startActivity} with intent \verb+i+. The action
associated with intent {\tt i} is {\tt \small SEND} and the  data type
is {\tt \small text/plain}. According to the
 manifest   in Figure~\ref{fig:icc-ex-manifest},  {\tt \small MsgAct}
 declares an \emph{intent filter} for {\tt \small SEND} actions on
data type {\tt \small text/plain}, meaning that {\tt \small msgAct} is a
target of the ICC site at line 10. Hence,  predicate 
{\tt \small
icc(MainAct, MsgAct, SEND, text/plain)
} is true.
\vspace{-0.06in}
\end{example} 

We now define the predicate {\tt icc*(P, Q)}, which is true if component {\tt P} can transitively launch component {\tt Q}:


\begin{definition}
\vspace{-0.05in}
Let \verb+icc_direct(P,Q)+ be a binary predicate which is true iff \verb+icc(P, Q, _, _)+ is true. The predicate {\tt icc*} is the reflexive transitive closure of \verb+icc_direct+.
\vspace{-0.05in}
\end{definition}

Our malware signature language includes the predicate {\tt icc*} because it allows writing signatures that are resilient to high-level control flow obfuscation. In particular, if the signature contains the predicate {\tt icc*(P, Q)}, adding or deleting dummy components for the purposes of detection evasion
will not affect the truth value of this predicate.







\subsubsection{Predicate {\tt calls}}
Apposcopy provides another control-flow predicate, called \verb+calls+, representing a relation of type
$
(comp : C, callee : M)
$
where domains $C$ and $M$ represent the set of all components and methods in the program respectively. 
Intuitively, \verb+calls(c,m)+ is true if method \verb+m+ is called by component \verb+c+.
More precisely,
\verb+calls+(C, M) is true iff $N$ is a life-cycle method defined in
component $C$ and $N \leadstostar  M$.

The \verb+calls+ predicate is useful for defining malware signatures  because it can be used to check if a component
calls Android API methods that can be abused by malware. Table~\ref{table:danger}
lists a few of such dangerous methods.



\subsubsection{Predicate {\tt flows}}

Apposcopy provides a data-flow predicate \verb+flows+  used for querying whether an app leaks
sensitive data. More specifically, taint flow  
 is defined in terms of sources and sinks.

\begin{definition}
\vspace{-0.05in}
A \emph{source} \emph{(resp. sink)} is a labeled (i.e., annotated) program variable that
 is either a method parameter or method return value. The corresponding method is referred to as the \emph{source method}
\emph{(resp. sink method)}.
\vspace{-0.05in}
\end{definition}

%We have manually annotated all sources that correspond to sensitive data in the Android platform 
%as well as all sinks which may potentially leak data. 
An example of a source is 
the return value of  method \verb+getDeviceId+, which yields the phone's unique device id. 
An example of a sink 
 is the third parameter of  \verb+sendTextMessage+, which
corresponds to the text to be sent through SMS. Source and sink annotations are discussed  in 
Section~\ref{sec:annotations},
and Table~\ref{table:source-sink} shows a partial list of source and sink methods.

\begin{table}
\vspace{-0.12in}
{\small 
\begin{center}
\caption{A non-exhaustive list of Android methods that are candidates of abuse}
\begin{tabular}{|l|}
  \hline
  {\bf Operation \& Description} \\
  \hline
  \texttt{<BroadcastReceiver: void abortBroadcast()>} \\
  \indent Block current broadcaster.  \\
  \hline
  \texttt{<Runtime: Process exec(java.lang.String)>}\\
  \indent Execute a command. \\
  \hline
  \texttt{<System: void loadLibrary(java.lang.String)>} \\
  \indent Perform native call. \\
  \hline
  \texttt{<PackageManager: List getInstalledPackages()>} \\
  \indent Get all application packages. \\
  \hline
  \texttt{<DexClassLoader: void <init>(...,ClassLoader)>} \\
   \indent Load classes from .jar and .apk files.\\
  \hline
\end{tabular}
\end{center}
}
\label{table:danger}
\vspace{-0.2in}
\end{table}

\begin{table*}
{\small 
\begin{center}
\caption{Examples of APIs with source and sink annotations}
\begin{tabular}{|l|l|}
  \hline
  {\bf Sources} & {\bf Sinks} \\
  \hline
  \texttt{<TelephonyManager: String getDeviceId()>} & \texttt{<SmsManager: void sendTextMessage(...)>}\\
 \hline
 \texttt{<TelephonyManager: String getSimSerialNumber()>} & \texttt{<SmsManager: void sendMultiparTextMessage(...)>} \\
  \hline
  \texttt{<TelephoneyManager: String getSubscriberId()>} & \texttt{<SmsManager: void sendDataMessage(...)>}\\
  \hline
  \texttt{<TelephoneyManager: String getVoiceMailNumber()>} & \texttt{<DataOutputStream: write(...)>}\\
  \hline
  \texttt{<TelephoneyManager: String getSimOperator()>} & \texttt{<DatagramSocket: void send(...)>}\\
  \hline
  \texttt{<Location: double getLatitude()>} & \texttt{<AbstractHttpClient: HttpResponse execute(...)>}\\
  \hline
  \texttt{<Location: double getLongitude()>} & \texttt{<Ndef: void writeNdefMessage(...)>}\\
  \hline
\end{tabular}
\end{center}
}
\label{table:source-sink}
\vspace{-0.2in}
\end{table*}


The \verb+flow+ predicate represents a relation of  type
$
(srcComp : C, src : {\rm SRC}, sinkComp : C, sink : {\rm SINK})
$
where domain $C$ is the set of components, and 
${\rm SRC}$ and ${\rm SINK}$ are the sets of all sources and  sinks in the program. 
To define the semantics of the \verb+flow+ predicate, we 
first define \emph{taint flow}:
\begin{definition}\label{def:taint-flow}
A \emph{taint flow} $(so,si)$ represents that a source 
labeled $so$ can flow to a sink labeled $si$ through a series of
assignments or matching loads and stores. % in fields of heap objects.
\end{definition}



%\begin{example}\label{ex:taint-flow}
%Consider the code snippet shown in Figure~\ref{fig:taint-ex}, where the return value of
%\verb+getDeviceId+ is a source labeled \verb+$getDeviceId+, and 
%the third parameter of  \verb+sendSMSMessage+ is a sink labeled \verb+!sendTextSMSMessage+.
%This application exhibits a taint flow from  \verb+$getDeviceId+ to 
%\verb+!sendTextSMSMessage+
%because variable \verb+y+ holding the return value of \verb+getDeviceId+ can 
%flow to variable \verb+v+ due to the chain of assignments, loads, and stores performed in
%lines 5-8.
%\end{example}


\begin{definition}
\vspace{-0.05in}
The predicate \verb+flow(p, so, q, si)+ is true iff (i) \verb+M+ and \verb+N+ are the source and
  sink methods corresponding to source \verb+so+ and sink \verb+si+,
  (ii) \verb+calls(p, M)+ and \verb+calls(q,N)+ are both true, and
  (iii) there exists a taint flow \verb+(so,si)+.
  \vspace{-0.05in}
\end{definition}


\begin{example}\label{ex:taint-flow}
Consider the code  in Figure~\ref{fig:taint-ex}, where the return value of
{\small \verb+getDeviceId+} is a source labeled {\small \verb+$getDeviceId+}, and 
the third parameter of  {\small \verb+sendTextMessage+} is a sink labeled {\small \verb+!sendTextMessage+}.
This application exhibits a taint flow from  {\small \verb+$getDeviceId+} to 
{\small \verb+!sendTextMessage+}
because variable \verb+y+ holding the return value of {\small \verb+getDeviceId+} can 
flow to variable \verb+v+ due to the chain of assignments, loads, and stores performed in
lines 5-8.
Hence, the following predicate evaluates to true:

{\small 
\begin{verbatim}
flow(ListDevice, $getDeviceId, ListDevice, !SendTextMessage)
\end{verbatim}
}
\end{example}






 
