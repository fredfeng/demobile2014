\section{Evaluation}

\begin{comment}
Our implementation  consists of about 30,000 lines of Java and uses several publicly-available software such as
Soot~\cite{Vallee-RaiCGHLS99} and bddbddb~\cite{WhaleyACL05}. Soot is used to
convert Android's .apk files to \emph{Jimple}, a higher-level
intermediate representation. 
A pre-processing step processes Jimple
instructions to extract various types of program facts, and our static
analyses are specified as Datalog programs. The bddbddb system takes
as input the Datalog specification of a static analysis and extracted
program facts and outputs the results of the  analysis.
Apposcopy's static analyses use manually-written models of the
Android framework; currently, we have models for about
1,100 classes that are relevant for our analyses.
\end{comment}



To evaluate the effectiveness and accuracy of Apposcopy, we
performed three sets of experiments, including evaluation on (i) known malware from the Android Malware Genome project~\cite{genome-project}, (ii) Google Play apps,  (iii) obfuscated malware.

%\begin{itemize}
%\item \emph{Evaluation on known malware:} We ran Apposcopy on several hundred known malware instances and evaluated whether it can classify these apps as %malware and identify their corresponding  families.
%\item \emph{Evaluation on Google Play apps:} We ran Apposcopy on several thousand apps from Google play to evaluate Apposcopy's false alarm rate.
%\item \emph{Evaluation on obfuscated malware:} We applied various kinds of obfuscations to known malware instances and compared the detection rate of Apposcopy with those of other anti-virus tools.
%\end{itemize}


\begin{comment}
\begin{table*}
\vspace{-0.1in}
    \centering
\caption{Examples of Apposcopy's signatures.}
{\scriptsize
    \begin{tabular}{ | l | l | }
  \hline
  Malware family &  Signature \\
  \hline
  ADRD &\texttt{ADRD :- receiver(r), icc(SYSTEM, r, BOOT\_COMPLETED, \_),receiver(s), service(t),icc*(r,s), icc*(s,t), icc*(t,s),} \\
       & \texttt{flow(t, DeviceId, t, ENC), flow(t, SubscriberId, t, ENC), flow(t, ENC, t, Internet).}\\
    \hline
    BeanBot & \texttt{BeanBot :- receiver(r), service(s), service(t), service(q), icc(SYSTEM, r, PHONE\_STATE, \_),} \\
            & \texttt{calls(r, abortBroadcast), icc*(r, s), icc*(s, t), icc*(s, q), flow(s, DeviceId, s, Internet),} \\
            & \texttt{flow(s, Line1Number, s, Internet), flow(s, SimSerialNumber, s, Internet).}\\
    \hline
    CoinPirate & \texttt{CoinPirate :- receiver(r), receiver(t), icc(SYSTEM, r, SMS\_SENT, \_), icc(SYSTEM, r, SMS\_RECEIVED, \_), service(s),} \\
               & \texttt{calls(r, abortBroadcast), calls(s, sendTextMessage), icc*(r, s), icc*(s, t), flow(s, DeviceId, s, Internet),}\\
               & \texttt{flow(s, SubscriberId, s, Internet), flow(s, Model, s, Internet), flow(s, SDK, s, Internet).} \\
   \hline
    \end{tabular}
    }
\label{table:query}
\vspace{-0.1in}
 \end{table*}


\subsection{Evaluation on Known Malware}\label{sec:malware-expts}

In our first experiment, we evaluate the effectiveness of Apposcopy on 1027 malware instances from the Android Malware Genome project~\cite{genome-project}, which contains real malware collected from various sources, including  Chinese and Russian third-party  app markets. 

All of these malicious applications belong to known malware families, such as DroidKungFu, Geinimi, and GoldDream.
To perform this experiment, we manually wrote specifications for the malware families included in the Android Malware Genome Project. For this purpose, we first read the relevant reports where available and  inspected a small number (1-5) of  instances for each malware family.

Table~\ref{table:genome} presents the results of this experiment. The third column and the fourth columns show the number of false negatives (FN) and false positives (FP) respectively. In this context, a false negative arises if an application $A$ belongs to a certain malware family $F$ but Apposcopy cannot detect that $A$ is an instance of $F$. Conversely, a false positive arises if an application $A$ does not belong to malware family $F$ but Apposcopy erroneously reports that it does. The final column of Table~\ref{table:genome} reports Apposcopy's overall accuracy, which is calculated as the number of correctly classified instances divided by the total number of samples.
 
 As shown in the last row of Table~\ref{table:genome}, the overall accuracy of Apposcopy over all malware instances that we analyzed is 90.0\%. That is, it can correctly classify  approximately 9 out of 10 malware instances accurately. 

 However, looking at the results more closely, we see that Apposcopy performs quite poorly on the BaseBridge malware family. Specifically, among the 121 samples that we analyzed, Apposcopy only classifies 46 of these applications as instances of BaseBridge. Upon further inspection  of this family, we found that many of the BaseBridge instances dynamically load the code that performs  malicious functionality.  Such behavior can inherently not be detected using static analysis and causes Apposcopy to yield many false negatives. Observe that, if we exclude BaseBridge from our samples, the overall accuracy of Apposcopy rises to 96.9\%.

 For the other malware families for which Apposcopy yields false negatives, there are several contributing factors. First, since we have written the specifications by inspecting only a small number of samples, our signatures may not adequately capture the essential characteristics of \emph{all} instances of that family. Second, the malware family may have some key feature that is not expressible in our malware specification language. For example, if a given malware performs malicious functionality without leaking sensitive data, Apposcopy will be unable to detect it. A third contributing factor for false negatives is due to missing models. Specifically, while our static analysis is sound, we do not analyze the underlying code of the Android system, but instead rely on \emph{method stubs} that capture the relevant behavior of the Android SDK. If these applications call SDK methods for which we have not provided stubs, Apposcopy may yield false negatives.
\end{comment} 
 
(i) The data from Table~\ref{table:genome} shows Apposcopy is able to detect Android malware with high accuracy of overall 90.0\%. More specifically, the false negative rate is around 10.0\% and the false
 positive ratio is less than 0.2\%. 
 
 \begin{comment}
 Finally, we remark that Apposcopy's analysis time on these malicious applications is moderate, with
 an average of 275 seconds per analyzed application containing 18,200 lines of Dalvik bytecode on average.
 \end{comment}

(ii) To verify that our high-level signatures also differentiate benign applications from real malware, we evaluated Apposcopy on 11,215 apps from Google Play and Apposcopy reported 16 of them as malware. We confirmed that those 16 apps are malware through VirusTotal~\cite{virus-total}. 

(iii) In the third experiment, we obfuscated existing malware using the ProGuard~\cite{proguard} tool and compared the detection rate of Apposcopy with other commercial anti-virus tools on obfuscated versions of known malware. The result shows that Apposcopy's detection rate is 100.0\% while the detection rates of other tools range from 14.3\% to 57.1\%.  
 
\begin{table}
    \vspace{-0.12in}
    \centering
    \caption{Evaluation of Apposcopy on malware from the Android Malware Genome project.}
    \small
    \begin{tabular}{|l|c|c|c|c|}
    \hline
    {\bf Malware Family}          & {\bf \#Samples} & {\bf FN} & {\bf FP} & {\bf Accuracy} \\ \hline
    DroidKungFu     & 444      & 15             & 0              & 96.6\%  \\ \hline
    AnserverBot     & 184      & 2              & 0              & 98.9\%  \\ \hline
    BaseBridge      & 121      & 75             & 0              & 38.0\%  \\ \hline
    Geinimi         & 68       & 2              & 2              & 97.1\%  \\ \hline
    DroidDreamLight & 46       & 0              & 0              & 100.0\%    \\ \hline
    GoldDream       & 46       & 1              & 0              & 97.8\%  \\ \hline
    Pjapps          & 43       & 7              & 0              & 83.7\%  \\ \hline
    ADRD            & 22       & 0              & 0              & 100.0\%    \\ \hline
    jSMSHider       & 16       & 0              & 0              & 100.0\%    \\ \hline
    DroidDream      & 14       & 1              & 0              & 92.9\%  \\ \hline
    Bgserv          & 9        & 0              & 0              & 100.0\%    \\ \hline
    BeanBot         & 8        & 0              & 0              & 100.0\%    \\ \hline
    GingerMaster    & 4        & 0              & 0              & 100.0\%    \\ \hline
    CoinPirate      & 1        & 0              & 0              & 100.0\%    \\ \hline
    DroidCoupon     & 1        & 0              & 0              & 100.0\%    \\ \hline \hline
    {\bf Total}     & 1027     & 103            & 2              & 90.0\%  \\ \hline
    \end{tabular}
\label{table:genome}
\vspace{-0.2in}
\end{table}

\begin{comment}
\begin{table*}
    \centering
    \caption{Result of top 4 Anti-virus companies for Genome.}
    \begin{tabular}{|l|l|l|l|l|}
    \hline
    \#Samples & AVG         & Lookout     & Norton      & Trend Micro \\ \hline
    1075     & 556(51.72\%) & 929(86.41\%) & 175(16.27\%) & 822(76.47\%) \\ \hline
    \end{tabular}
\end{table*}
\end{comment}

\begin{comment}
\subsection{Evaluation on Google Play Apps}\label{sec:google-play}

In a second experiment, we evaluate Apposcopy on thousands of apps from Google Play. Since these applications are
 available through the official Android  market rather than less reliable third-party app markets, we would expect a large majority of these applications to be benign. Hence, by running Apposcopy on Google Play apps, we can assess whether our high-level  signatures adequately differentiate benign applications from real malware. 

In our experiment, among the 11,215 apps analyzed by Apposcopy, only 16 of them were reported as malware. Specifically, Apposcopy reported two applications to be instances of  DroidDreamLight, one to be an instance of DroidDream and another one to be an instance of Pjapps. The remaining 12 applications were categorized as  DroidKungFu. 
To decide whether these 16 apps are indeed malware, we uploaded them to  VirusTotal~\cite{virus-total} for analyzing suspicious applications. VirusTotal is a service that runs multiple anti-virus tools on the uploaded application and shows their aggregate results. Based on the results provided by VirusTotal, the majority of anti-virus tools agree with Apposcopy's classification for 13 of the 16 reported malware.  For the remaining three applications, the majority of the tools classify them as malicious adware while Apposcopy classifies them as instances of  DroidKungFu. 
This experiment confirms our claim that Apposcopy does not generate a
lot of false alarms and  that our  malware signatures can
 distinguish benign applications from real malware.
 
 Similar to the experiments from Section~\ref{sec:malware-expts}, Apposcopy takes an average of 346 seconds to analyze a Google Play application with 26,786 lines of Dalvik bytecode on average.

\begin{table*}
    \centering
    \caption{Result of 8528 applications in Google Play.}
    \begin{tabular}{|l|l|l|l|l|}
    \hline
    Source      & \#Samples & False Positive & False Negative & Accuracy \\ \hline
    Google Play &  8528     & 0              & 0              & 100\%    \\ \hline
    \end{tabular}
\end{table*}
\end{comment}

\begin{comment}
\subsection{Evaluation on Obfuscated Apps}

\begin{table*}
  \centering
  \caption{Comparison between Apposcopy and other tools on obfuscated malware.} \label{table:exper3}
  \small
  \begin{tabular}{|l|c|c|c|c|c|c|c|c|}
    \hline
    Family          & AVG & Symantec & ESET & Dr. Web & Kaspersky & Trend Micro & McAfee & Apposcopy \\ \hline
    DroidKungFu     &\xmark &\xmark  &\cmark&\xmark   &\cmark     &\xmark       &\xmark  &\cmark          \\  \hline
    Geinimi         & \xmark&\xmark  &\xmark&\xmark   &\xmark     &\xmark       &\xmark  &\cmark          \\  \hline
    DroidDreamLight & \xmark&\xmark  &\xmark&\xmark   &\xmark     &\xmark       &\xmark  &\cmark          \\  \hline
    GoldDream       & \xmark&\xmark  &\xmark&\xmark   &\xmark     &\cmark       &\xmark  &\cmark          \\  \hline
    DroidDream      &\cmark &\cmark  &\cmark&\cmark   &\cmark     &\cmark       &\cmark  &\cmark          \\ \hline 
    BeanBot         & \xmark&\xmark  &\xmark&\xmark   &\xmark     &\xmark       &\xmark  &\cmark          \\   \hline
    GingerMaster    & \xmark&\xmark  &\cmark&\cmark   &\cmark     &\cmark       &\cmark  &\cmark          \\   \hline
    Pjapps          & \xmark&\xmark  &\xmark&\xmark   &\xmark     &\xmark       &\xmark  &\cmark          \\   \hline
    Bgserv          & \cmark&\xmark  &\xmark&\xmark   &\cmark     &\xmark       &\xmark  &\cmark          \\   \hline
    CoinPirate      & \xmark&\xmark  &\xmark&\xmark   &\cmark     &\xmark       &\xmark  &\cmark          \\  \hline
    jSMSHider       & \cmark&\cmark  &\cmark&\cmark   &\cmark     &\cmark       &\cmark  &\cmark          \\   \hline
    AnserverBot     & \cmark&\xmark  &\cmark&\cmark   &\cmark     &\cmark       &\cmark  &\cmark          \\   \hline
    DroidCoupon     & \xmark&\xmark  &\cmark&\cmark   &\cmark     &\xmark       &\xmark  &\cmark          \\  \hline
    ADRD            & \xmark&\xmark  &\xmark&\xmark   &\xmark     &\xmark       &\xmark  &\cmark          \\  \hline
    \hline
    Success rate    &28.6\% &14.3\%  &42.9\% &35.7\%  &57.1\%     &35.7\%       &28.6\%  &100.0\%           \\  \hline
  \end{tabular}
  \vspace{-0.1in}
\end{table*}
%While the previous  experiments demonstrate that Apposcopy can accurately pinpoint apps that belong to known malware families, these experiments do not %illustrate how Apposcopy adds value over existing malware detection tools. 
To substantiate our claim that Apposcopy is  resilient to code transformations, we compare the detection rate of Apposcopy with other anti-virus tools on obfuscated versions of known malware.
For this experiment, we obfuscated existing malware using the ProGuard tool~\cite{proguard}, which is commonly used by malware writers to 
evade detection. In addition, since the databases of some of the anti-virus tools include signatures of malware samples obfuscated by ProGuard, we also applied three additional   obfuscations that are not performed by ProGuard:
First, our obfuscator changes the names of components,
classes, methods, and
fields. 
%To preserve semantics, we also transform the code
%that operates on string constants representing names of classes and
%update the AndroidManifest.xml. 
Second, all invocations to methods of
\texttt{android.*} classes are redirected through proxy methods.
Third, our obfuscator also performs string encryption, including encryption of component names as well as action and data type values of intents.

%In this experiment, we use the above obfuscator to obfuscate known malware.
Table~\ref{table:exper3} shows the results of our third experiment on obfuscated malware. Each row in this table corresponds to an application that is an instance of a known malware family (and whose unobfuscated version can be identified as malware by all tools considered in our evaluation). Each column in the table corresponds to a leading anti-virus tool, namely, AVG, Symantec, ESET, Dr. Web, Kaspersky, Trend Micro, and McAfee. A check (\cmark) indicates that the tool is able to detect the obfuscated version of the program as malware, and a cross (\xmark) means that the tool is unable to classify the obfuscated version as malicious.
As Table~\ref{table:exper3} shows, Apposcopy is resistant to these obfuscations for all malware that we considered. In contrast, none of the other tools can successfully classify all of the obfuscated apps as malware. 
%Hence, this experiment shows that Apposcopy is indeed more resilient to low-level code transformations than other signature-based tools. 

\subsection{Comparison with Kirin}
In addition to comparing Apposcopy with commercial anti-virus tools, we also compared Apposcopy against Kirin~\cite{EnckOM09}, which is the \emph{only} publicly available research tool for Android malware detection. As explained in Section~\ref{sec:related}, Kirin is a signature-based malware detector that classifies an app as malware if it uses a dangerous combination of permissions specified by the malware signature. On the set of malicious apps considered in Section~\ref{sec:malware-expts}, Kirin reports only 532 apps out of 1,027 malicious apps to be malware. This corresponds to a false negative rate of 48\% , which is quite high compared to the 10\% false negative rate of Apposcopy. On the other hand, for the set of applications taken from Google Play and considered in Section~\ref{sec:google-play}, Kirin reports 8\% of these apps to be malware, while Apposcopy classifies only 0.14\% of these apps as malicious. We manually inspected 20 out of the 886 apps classified as malware by the Kirin tool and also compared with the results of VirusTotal. Our evaluation revealed that the overwhelming majority of the apps classified as malware by Kirin are false positives. Hence, our experiments demonstrate that Apposcopy outperforms Kirin both in terms of false positives as well as false negatives.



To compare Apposcopy with other research tools, we run the above data set on Kirin~\cite{EnckOM09}, which checks whether the permission of applications 
violates a set of permission rules as the signal of malcious activity.
Kirin reports 532 malware out of the 1,027 known malware and 886 malware out of the 11,215 apps from Google Play. Out result shows that Apposcopy significantly outperforms Kirin with lower false positive and false negtive rate.
We also tried to contact with other researchers and ask for their recent tools but most of them didn't reply. 
\end{comment}
%\section{Case studies}
%TBD.
